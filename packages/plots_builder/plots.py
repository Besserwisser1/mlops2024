import os
import warnings

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

os.environ["SHAPE_RESTORE_SHX"] = "YES"
warnings.filterwarnings("ignore")


def show_barplot_nan(nan_dataframe: pd.DataFrame):
    """_summary_

    Args:
        nan_dataframe (pd.DataFrame): _description_
    Return:
        1 chart of stats for nan values
    """
    plt.figure(figsize=(10, 6))
    sns.set_style("whitegrid")
    sns.barplot(x="column", y="nan_number_percent", data=nan_dataframe, color="skyblue")
    plt.title("Процент пропущенных значений по столбцам")
    plt.xlabel("Название столбца")
    plt.ylabel("Процент пропущенных значений")
    plt.xticks(rotation=45, ha="right")
    plt.tight_layout()
    plt.show()


def show_pieplots_nan(full_data, nan_dataframe):
    """_summary_

    Args:
        full_data (_type_): _description_
        nan_dataframe (_type_): _description_
    Return:
        n pie charts of nan
    """
    dfs_for_nan_plots = {}
    for col in nan_dataframe["column"].unique():
        nan_percentage = full_data[col].isna().mean() * 100
        nan_df = pd.DataFrame(
            {
                "value": ["Non-NaN", "NaN"],
                "percent": [100 - nan_percentage, nan_percentage],
            }
        )
        dfs_for_nan_plots[col] = nan_df
    for col, nan_df in dfs_for_nan_plots.items():
        labels = list(nan_df["value"])
        values = nan_df["percent"]

        plt.figure(figsize=(6, 6))
        plt.pie(values, labels=labels, autopct="%1.1f%%", startangle=140)
        plt.title(f'Процент пропущенных значений в столбце "{col}"')
        plt.axis("equal")
        plt.show()
