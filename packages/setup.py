from setuptools import setup, find_packages


setup(
    name="py_plot_builder",
    version="1.0.4",
    author="einbesserwisser1",
    author_email="einbesserwisser@yandex.ru",
    description="A demo py package to deploy on gitlab",
    license="MIT",
    packages=find_packages(),
    classifiers=["Python 3.9", "GitLab"],
)
